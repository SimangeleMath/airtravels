package sima.co.za.travel_airports.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;


import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import sima.co.za.travel_airports.airport.AirportViewModel;
import sima.co.za.travel_airports.airport.schedule.DepartureViewModel;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;

@Module
public abstract class ViewModelModule {
    @Binds
    public abstract ViewModelProvider.Factory bindsViewModelFactory(ViewModelFactory aViewModelFactory);

    @Binds
    @IntoMap
    @ViewModelKey(AirportViewModel.class)
    public abstract ViewModel provideAirportViewModel(AirportViewModel aAirportViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(DepartureViewModel.class)
    public abstract ViewModel provideAirportScheduleViewModel(DepartureViewModel aDepartureViewModel);

}
