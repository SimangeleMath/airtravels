package sima.co.za.travel_airports.airport.schedule;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;
import sima.co.za.travel_airports.airport.schedule.models.CityModel;
import sima.co.za.travel_airports.models.AirportScheduleRequestModel;
import sima.co.za.travel_airports.services.TravelAirportApiService;
import sima.co.za.travel_airports.utils.Constants;

public class DepartureRepository {

    private String TAG = DepartureRepository.class.getSimpleName();
    private final Application mApplication;
    private final TravelAirportApiService mAirportApiService;
    private AirportModel mAirportModel;
    private final MutableLiveData<List<AirportScheduleModel>> mMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<CityModel> mCityMutableLiveData = new MutableLiveData<>();

    @Inject
    public DepartureRepository(Application aApplication, TravelAirportApiService aAirportApiService, AirportModel aAirportModel) {
        mApplication = aApplication;
        mAirportApiService = aAirportApiService;
        mAirportModel = aAirportModel;
    }

    public void getAirportSchedule(AirportScheduleRequestModel aScheduleRequestModel) {
        try {

            mAirportApiService.executeGetAirportSchedule(aScheduleRequestModel.getKey(), aScheduleRequestModel.getIataCode(), aScheduleRequestModel.getType())
                    .enqueue(new Callback<List<AirportScheduleModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<AirportScheduleModel>> call, @NonNull Response<List<AirportScheduleModel>> response) {
                            try {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        mMutableLiveData.setValue(response.body());
                                    }
                                }
                            } catch (Exception ex) {
                                if (Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<AirportScheduleModel>> call, @NonNull Throwable t) {
                            if (Constants.showDebugMessages) Log.e(TAG, t.getMessage());
                            //TODO callback
                            Toast.makeText(mApplication, "Error while connecting to server", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception ex) {
            if (Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
        }
    }

    public void getCity(AirportScheduleRequestModel aScheduleRequestModel, String codeIataCity) {
        try {
            mAirportApiService.executeGetCity(aScheduleRequestModel.getKey(), codeIataCity)
                    .enqueue(new Callback<List<CityModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<CityModel>> call, @NonNull Response<List<CityModel>> response) {
                            try {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        if (response.body().size() > 0) {
                                            mCityMutableLiveData.setValue(response.body().get(0));
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                if (Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<CityModel>> call, @NonNull Throwable t) {
                            if (Constants.showDebugMessages) Log.e(TAG, t.getMessage());
                            //TODO callback
                            Toast.makeText(mApplication, "Error while connecting to server", Toast.LENGTH_SHORT).show();
                        }
                    });
        } catch (Exception ex) {
            if (Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
        }
    }


    MutableLiveData<List<AirportScheduleModel>> getMutableLiveData() {
        return mMutableLiveData;
    }

    public MutableLiveData<CityModel> getCityMutableLiveData() {
        return mCityMutableLiveData;
    }
}
