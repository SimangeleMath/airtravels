package sima.co.za.travel_airports.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import sima.co.za.travel_airports.activities.MainActivity;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract MainActivity contributeMainActivity();
}
