package sima.co.za.travel_airports.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import sima.co.za.travel_airports.BR;

public class AirportScheduleRequestModel extends BaseObservable {

    private String mKey;
    private String mIataCode;
    private String mType;

    public AirportScheduleRequestModel(String aKey, String aIataCode, String aType) {
        mKey = aKey;
        mIataCode = aIataCode;
        mType = aType;
    }

    @Bindable
    public String getKey() {
        return mKey;
    }

    public void setKey(String aKey) {
        mKey = aKey;
        notifyPropertyChanged(BR.key);
    }

    @Bindable
    public String getIataCode() {
        return mIataCode;
    }

    public void setIataCode(String aIataCode) {
        mIataCode = aIataCode;
        notifyPropertyChanged(BR.iataCode);
    }

    @Bindable
    public String getType() {
        return mType;
    }

    public void setType(String aType) {
        mType = aType;
        notifyPropertyChanged(BR.type);
    }

}
