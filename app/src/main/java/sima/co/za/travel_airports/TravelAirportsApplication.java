package sima.co.za.travel_airports;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.support.v4.app.Fragment;

import com.facebook.stetho.Stetho;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import dagger.android.HasServiceInjector;
import dagger.android.support.HasSupportFragmentInjector;
import sima.co.za.travel_airports.di.AppModule;
import sima.co.za.travel_airports.di.AppComponent;
import sima.co.za.travel_airports.di.DaggerAppComponent;
import sima.co.za.travel_airports.di.RepositoryModule;
import sima.co.za.travel_airports.di.ServiceBuilderModule;

public class TravelAirportsApplication extends Application implements HasSupportFragmentInjector, HasActivityInjector, HasServiceInjector {

    @Inject
    DispatchingAndroidInjector<Fragment> mDispatchingAndroidInjector;
    @Inject
    DispatchingAndroidInjector<Activity> mDispatchingAndroidActivityInjector;
    @Inject
    DispatchingAndroidInjector<Service> dispatchingServiceInjector;

    @Override
    public void onCreate() {
        super.onCreate();
        AppComponent appComponent = DaggerAppComponent.builder()
                .application(this)
                .appModule(new AppModule(this))
                .repoModule(new RepositoryModule())
                .serviceModule(new ServiceBuilderModule())
                .build();

        appComponent.inject(this);
        Stetho.initializeWithDefaults(this);
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return mDispatchingAndroidInjector;
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return mDispatchingAndroidActivityInjector;
    }

    @Override
    public AndroidInjector<Service> serviceInjector() {
        return dispatchingServiceInjector;
    }
}
