package sima.co.za.travel_airports.airport.schedule;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import java.util.List;

import javax.inject.Inject;

import sima.co.za.travel_airports.BuildConfig;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;
import sima.co.za.travel_airports.airport.schedule.models.CityModel;
import sima.co.za.travel_airports.models.AirportScheduleRequestModel;

public class DepartureViewModel extends ViewModel{

    @Inject
    AirportModel mAirportModel;
    private final String TAG = DepartureViewModel.class.getSimpleName();
    private final Application mApplication;
    private final DepartureRepository mDepartureRepository;
    private final MutableLiveData<List<AirportScheduleModel>> mMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<CityModel> mCityMutableLiveData = new MutableLiveData<>();

    private final Observer<List<AirportScheduleModel>> response = new Observer<List<AirportScheduleModel>>() {
        @Override
        public void onChanged(@Nullable List<AirportScheduleModel> aResponse) {
            if (aResponse != null) {
                mMutableLiveData.setValue(aResponse);
            }
        }
    };

    private final Observer<CityModel> cityResponse = new Observer<CityModel>() {
        @Override
        public void onChanged(@Nullable CityModel aResponse) {
            if (aResponse != null) {
                mCityMutableLiveData.setValue(aResponse);
            }
        }
    };

    @Inject
    public DepartureViewModel(Application application, DepartureRepository aDepartureRepository) {
        mApplication = application;
        mDepartureRepository = aDepartureRepository;
        mDepartureRepository.getMutableLiveData().observeForever(response);
        mDepartureRepository.getCityMutableLiveData().observeForever(cityResponse);
    }

    public void getDepartureSchedule(String aCde){
        mDepartureRepository.getAirportSchedule(new AirportScheduleRequestModel(BuildConfig.API_KEY, aCde,"departure"));
    }

    public void getCity(String aCode, String aCity){
        mDepartureRepository.getCity(new AirportScheduleRequestModel(BuildConfig.API_KEY, aCode,"departure"),aCity );
    }

    public MutableLiveData<List<AirportScheduleModel>> getMutableLiveData() {
        return mMutableLiveData;
    }

    public MutableLiveData<CityModel> getCityMutableLiveData() {
        return mCityMutableLiveData;
    }
}
