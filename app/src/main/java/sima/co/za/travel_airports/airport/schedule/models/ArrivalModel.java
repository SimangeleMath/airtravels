package sima.co.za.travel_airports.airport.schedule.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArrivalModel {

    @SerializedName("iataCode")
    @Expose
    private String mIataCode;
    @SerializedName("icaoCode")
    @Expose
    private String mIcaoCode;
    @SerializedName("scheduledTime")
    @Expose
    private String mScheduledTime;
    @SerializedName("estimatedTime")
    @Expose
    private String mEstimatedTime;
    @SerializedName("actualTime")
    @Expose
    private String mActualTime;

    public String getIataCode() {
        return mIataCode;
    }

    public void setIataCode(String aIataCode) {
        mIataCode = aIataCode;
    }

    public String getIcaoCode() {
        return mIcaoCode;
    }

    public void setIcaoCode(String aIcaoCode) {
        mIcaoCode = aIcaoCode;
    }

    public String getScheduledTime() {
        return mScheduledTime;
    }

    public void setScheduledTime(String aScheduledTime) {
        mScheduledTime = aScheduledTime;
    }

    public String getEstimatedTime() {
        return mEstimatedTime;
    }

    public void setEstimatedTime(String aEstimatedTime) {
        mEstimatedTime = aEstimatedTime;
    }

    public String getActualTime() {
        return mActualTime;
    }

    public void setActualTime(String aActualTime) {
        mActualTime = aActualTime;
    }

}