package sima.co.za.travel_airports.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import sima.co.za.travel_airports.R;


public class Utils {

    /**
     * Checks for Network & Internet Connectivity
     *
     * @param mContext current context
     * @return true if internet available
     */

    public static final Locale LOCALE;

    static {
        LOCALE = Locale.US;
    }

    public boolean isNetworkAvailable(Context mContext) {
        try {
            ConnectivityManager cm =
                    (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = null;
            if (cm != null) {
                activeNetwork = cm.getActiveNetworkInfo();
            }
            return activeNetwork != null && activeNetwork.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    //date convert with time zone
    public String formatTimestampWithoutTimezone(String inDate, String format) {
        if (!TextUtils.isEmpty(inDate)) {
            try {
                if (inDate.contains(".")) {
                    String substring = inDate.substring(inDate.indexOf("."), inDate.indexOf("+"));
                    inDate = inDate.replace(substring, "");
                }
                if (inDate.contains("+")) {
                    String timeZone = inDate.substring(inDate.lastIndexOf("+") + 1);
                    if (timeZone.length() == 2) {
                        inDate = inDate.concat("00");
                    }
                }
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.TIMESTAMP_FORMAT, Locale.US);
                Date outDate = simpleDateFormat.parse(inDate);
                simpleDateFormat.applyPattern(format);
                TimeZone tz = TimeZone.getDefault();
                simpleDateFormat.setTimeZone(tz);
                return simpleDateFormat.format(outDate);
            } catch (Exception e) {
                if (Constants.showStackTrace) e.printStackTrace();
            }
        }
        return "";
    }


    public static void showDialog(final Context aContext, String aTitle, String aMessage) {
        new AlertDialog.Builder(aContext)
                .setTitle(aTitle)
                .setMessage(aMessage)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public String formatTime(String dateTime) throws ParseException {
        SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat("yyyy-dd-mm'T'HH:mm:ss", Locale.ENGLISH);
        Date date = simpleDateFormat1.parse(dateTime);
        System.out.println(date);
        SimpleDateFormat simpleDateFormat2 = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
        return simpleDateFormat2.format(date);
    }
}
