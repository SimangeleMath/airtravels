package sima.co.za.travel_airports.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import sima.co.za.travel_airports.BR;

public class AirportRequestModel extends BaseObservable {

    private String mKey;
    private double mLat;
    private double mLng;
    private int mDistance;

    public AirportRequestModel(String aKey, double aLat, double aLng, int aDistance) {
        mKey = aKey;
        mLat = aLat;
        mLng = aLng;
        mDistance = aDistance;
    }

    @Bindable
    public String getKey() {
        return mKey;
    }

    public void setKey(String aKey) {
        mKey = aKey;
        notifyPropertyChanged(BR.key);
    }

    @Bindable
    public double getLat() {
        return mLat;
    }

    public void setLat(double aLat) {
        mLat = aLat;
        notifyPropertyChanged(BR.lat);
    }

    @Bindable
    public double getLng() {
        return mLng;
    }

    public void setImei(double aLng) {
        mLng = aLng;
        notifyPropertyChanged(BR.lng);
    }

    @Bindable
    public int getDistance() {
        return mDistance;
    }

    public void setDistance(int aDistance) {
        mDistance = aDistance;
        notifyPropertyChanged(BR.distance);
    }

}
