package sima.co.za.travel_airports.fragments;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.navigation.NavOptions;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import sima.co.za.travel_airports.R;
import sima.co.za.travel_airports.base.BaseFragment;
import sima.co.za.travel_airports.databinding.FragmentSplashBinding;
import sima.co.za.travel_airports.utils.Constants;

public class SplashFragment extends BaseFragment implements EasyPermissions.PermissionCallbacks {

    private FragmentSplashBinding mBinding;

    public static final int REQUEST_CODE_ALL_PERMISSION = 1;
    private static String[] PERMISSIONS_REQUEST_ALL = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSplashBinding.inflate(inflater,container,false);
        return mBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Animation mAnimation = AnimationUtils.loadAnimation(requireContext(),
                R.anim.move);
        mAnimation.setDuration(1000);
        mBinding.imageView.startAnimation(mAnimation);

        initialize();
    }

    private void initialize(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                methodRequestAllPermissions();
            }
        }, 2000);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //REQUEST RESULTS
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_CODE_ALL_PERMISSION)
    private void methodRequestAllPermissions() {
        try {
        if (EasyPermissions.hasPermissions(requireContext(), PERMISSIONS_REQUEST_ALL)) {
            navigate();
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.msg_error_needs_location_permissions), REQUEST_CODE_ALL_PERMISSION, PERMISSIONS_REQUEST_ALL);
        }
        }catch (Exception ex){
            if(Constants.showErrorMessages){
            }
        }
    }


    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        navigate();
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            EasyPermissions.requestPermissions(this, getString(R.string.msg_error_needs_location_permissions), REQUEST_CODE_ALL_PERMISSION, PERMISSIONS_REQUEST_ALL);
        }else{
            requireActivity().finish();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AppSettingsDialog.DEFAULT_SETTINGS_REQ_CODE) {
            if(Constants.debugging) Log.d("Location","back from settings");
        }
    }

    private void navigate(){
        navigateToNextScreen(R.id.action_splashFragment_to_mainFragment, new NavOptions.Builder().setPopUpTo(R.id.splashFragment, true).build());
    }
}
