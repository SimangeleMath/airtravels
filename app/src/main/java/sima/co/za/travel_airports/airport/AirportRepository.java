package sima.co.za.travel_airports.airport;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sima.co.za.travel_airports.models.AirportRequestModel;
import sima.co.za.travel_airports.services.TravelAirportApiService;
import sima.co.za.travel_airports.utils.Constants;
import sima.co.za.travel_airports.utils.DialogHelper;

public class AirportRepository {

    private String TAG = AirportRepository.class.getSimpleName();
    private final Application mApplication;
    private final TravelAirportApiService mAirportApiService;
    private AirportRequestModel mAirportRequestModel;
    private final MutableLiveData<List<AirportModel>> mMutableLiveData = new MutableLiveData<>();
    private DialogHelper mCustomProgressDialog;

    @Inject
    public AirportRepository(Application aApplication, TravelAirportApiService aAirportApiService) {
        mApplication = aApplication;
        mAirportApiService = aAirportApiService;
    }

    public void getAirports(AirportRequestModel aAirportRequestModel) {
        try {
            mAirportApiService.executeGetNearByAirports(aAirportRequestModel.getKey(), aAirportRequestModel.getLat(), aAirportRequestModel.getLng(), aAirportRequestModel.getDistance())
                    .enqueue(new Callback<List<AirportModel>>() {
                        @Override
                        public void onResponse(@NonNull Call<List<AirportModel>> call, @NonNull Response<List<AirportModel>> response) {
                            try {
                                if (response.isSuccessful()) {
                                    if (response.body() != null) {
                                        mMutableLiveData.setValue(response.body());
                                    }
                                }
                            } catch (Exception ex) {
                                 if (Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<List<AirportModel>> call, @NonNull Throwable t) {
                            if (Constants.showDebugMessages) Log.e(TAG, t.getMessage());
                            //TODO callback
                            Toast.makeText(mApplication, "Error while connecting to server", Toast.LENGTH_SHORT).show();
                        }
                    });
        }catch (Exception ex){
            if(Constants.showErrorMessages) Log.e(TAG, ex.getMessage());
        }
    }


    MutableLiveData<List<AirportModel>> getMutableLiveData() {
        return mMutableLiveData;
    }

}
