package sima.co.za.travel_airports.di;

import dagger.Module;
import dagger.Provides;
import sima.co.za.travel_airports.services.LocationService;

@Module
public class ServiceBuilderModule {

    @Provides
     LocationService provideLocationService(){
        return new LocationService();
    }
}
