package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirportScheduleModel {

    @SerializedName("type")
    @Expose
    private String mType;

    @SerializedName("status")
    @Expose
    private String mStatus;

    @SerializedName("departure")
    @Expose
    private DepartureModel mDepartureModel;

    @SerializedName("arrival")
    @Expose
    private ArrivalModel mArrivalModel;

    @SerializedName("airline")
    @Expose
    private AirLineModel mAirlineModel;

    @SerializedName("flight")
    @Expose
    private FlightModel mFlightModel;

    @SerializedName("codeshared")
    @Expose
    private CodeSharedModel mCodeSharedModel;

    public String getType() {
        return mType;
    }

    public void setType(String aType) {
        mType = aType;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String aStatus) {
        mStatus = aStatus;
    }

    public DepartureModel getDeparture() {
        return mDepartureModel;
    }

    public void setDeparture(DepartureModel aDepartureModel) {
        mDepartureModel = aDepartureModel;
    }


    public ArrivalModel getArrival() {
        return mArrivalModel;
    }

    public void setArrival(ArrivalModel aArrivalModel) {
        mArrivalModel = aArrivalModel;
    }

    public AirLineModel getAirline() {
        return mAirlineModel;
    }

    public void setAirline(AirLineModel aAirlineModel) {
        mAirlineModel = aAirlineModel;
    }

    public FlightModel getFlight() {
        return mFlightModel;
    }

    public void setFlight(FlightModel aFlightModel) {
        mFlightModel = aFlightModel;
    }

    public CodeSharedModel getCodeSharedModel() {
        return mCodeSharedModel;
    }

    public void setCodeSharedModel(CodeSharedModel aCodeSharedModel) {
        mCodeSharedModel = aCodeSharedModel;
    }
}
