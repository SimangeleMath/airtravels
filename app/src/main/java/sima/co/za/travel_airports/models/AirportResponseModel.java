package sima.co.za.travel_airports.models;

import java.util.List;

import sima.co.za.travel_airports.airport.AirportModel;

public class AirportResponseModel{

    private List<AirportModel> result;

    public List<AirportModel> getResult() {
        return result;
    }

    public void setResult(List<AirportModel> aResult) {
        result = aResult;
    }

    @Override
    public String toString() {
        return "AirportModel{" +
                "result=" + result +
                '}';
    }
}
