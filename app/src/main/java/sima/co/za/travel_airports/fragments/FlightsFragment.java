package sima.co.za.travel_airports.fragments;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import sima.co.za.travel_airports.R;
import sima.co.za.travel_airports.adapters.FlightAdapter;
import sima.co.za.travel_airports.airport.schedule.DepartureViewModel;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;
import sima.co.za.travel_airports.airport.schedule.models.CityModel;
import sima.co.za.travel_airports.base.BaseFragment;
import sima.co.za.travel_airports.databinding.FragmentFlightsBinding;
import sima.co.za.travel_airports.di.ViewModelFactory;
import sima.co.za.travel_airports.utils.DialogHelper;
import sima.co.za.travel_airports.utils.Utils;

public class FlightsFragment extends BaseFragment {

    @Inject
    ViewModelFactory mFactory;
    private FragmentFlightsBinding mBinding;
    private DepartureViewModel mDepartureViewModel;
    private DialogHelper mCustomProgressDialog;
    private FlightsFragmentArgs mFragmentArgs;

    @Inject
    Utils mUtils;

    @Override
    public void onCreate(@Nullable Bundle aSavedInstanceState) {
        super.onCreate(aSavedInstanceState);
        mDepartureViewModel = ViewModelProviders.of(this, mFactory).get(DepartureViewModel.class);
       // mCustomProgressDialog = new DialogHelper(requireContext());
       // mUtils = new Utils();

        if (getArguments() != null) {
            mFragmentArgs = FlightsFragmentArgs.fromBundle(getArguments());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentFlightsBinding.inflate(inflater, container, false);
        final FlightAdapter mFlightAdapter = new FlightAdapter();

        mDepartureViewModel.getMutableLiveData().observe(this, new Observer<List<AirportScheduleModel>>() {
            @Override
            public void onChanged(@Nullable List<AirportScheduleModel> airportScheduleModels) {
                if (airportScheduleModels != null) {
                   // if (mCustomProgressDialog.isProgressDialogVisible())
                    //    mCustomProgressDialog.dismissProgress();
                    mFlightAdapter.setScheduleList(airportScheduleModels);
                }
            }
        });

        mDepartureViewModel.getCityMutableLiveData().observe(this, new Observer<CityModel>() {
            @Override
            public void onChanged(@Nullable CityModel cityModel) {
                mFlightAdapter.setCity(cityModel);
            }
        });

       // mCustomProgressDialog.showProgress();
        if (!mUtils.isNetworkAvailable(requireActivity())) {
            Utils.showDialog(requireContext(), "test", "test");
       //     mCustomProgressDialog.dismissProgress();
        } else {
            mDepartureViewModel.getDepartureSchedule(mFragmentArgs.getArgCode());
            mDepartureViewModel.getCity(mFragmentArgs.getArgCityCode(), mFragmentArgs.getArgCityCode());
        }

        mBinding.rvAirports.setAdapter(mFlightAdapter);
        mBinding.rvAirports.addItemDecoration(new DividerItemDecoration(requireActivity(), DividerItemDecoration.VERTICAL));
        mFlightAdapter.setScheduleList(new ArrayList<AirportScheduleModel>());

        return mBinding.getRoot();
    }
}
