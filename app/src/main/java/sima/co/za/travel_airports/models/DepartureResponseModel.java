package sima.co.za.travel_airports.models;

import java.util.List;

import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;

public class DepartureResponseModel {

    private List<AirportScheduleModel> result;

    public List<AirportScheduleModel> getResult() {
        return result;
    }

    public void setResult(List<AirportScheduleModel> aResult) {
        result = aResult;
    }

    @Override
    public String toString() {
        return "AirportScheduleModel{" +
                "result=" + result +
                '}';
    }
}
