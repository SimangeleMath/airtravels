package sima.co.za.travel_airports.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

public class PreferencesHelper {

    // PREFS
    private static final String PREFS_NAME = "sima.co.za.travel_airports.prefs";

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor prefsEditor;

    @SuppressLint("CommitPrefEdits")
    public PreferencesHelper(Context context) {
        this.sharedPreferences = context.getSharedPreferences(PREFS_NAME, Activity.MODE_PRIVATE);
        this.prefsEditor = sharedPreferences.edit();
    }

    public Double getCoordinate(String key) {
        String aDouble =
         sharedPreferences.getString(key, null);
        if (aDouble != null) {
            return Double.valueOf(aDouble);
        }
        return 0.0;
    }

    public void putCo(String key, String value) {
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public String getString(String key) {
        return sharedPreferences.getString(key, "");
    }

    public String getString(String key, String aDefaultValue) {
        return sharedPreferences.getString(key, aDefaultValue);
    }

}
