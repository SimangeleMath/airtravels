package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirLineModel {

    @SerializedName("name")
    @Expose
    private String mName;
    @SerializedName("iataCode")
    @Expose
    private String mIataCode;
    @SerializedName("icaoCode")
    @Expose
    private String mIcaoCode;

    public String getName() {
        return mName;
    }

    public void setName(String aName) {
        mName = aName;
    }

    public String getIataCode() {
        return mIataCode;
    }

    public void setIataCode(String aIataCode) {
        mIataCode = aIataCode;
    }

    public String getIcaoCode() {
        return mIcaoCode;
    }

    public void setIcaoCode(String aIcaoCode) {
        mIcaoCode = aIcaoCode;
    }

}