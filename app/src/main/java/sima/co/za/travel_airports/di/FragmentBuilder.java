package sima.co.za.travel_airports.di;


import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import sima.co.za.travel_airports.base.BaseFragment;
import sima.co.za.travel_airports.fragments.FlightsFragment;
import sima.co.za.travel_airports.fragments.MapFragment;
import sima.co.za.travel_airports.fragments.SplashFragment;

@Module
public abstract class FragmentBuilder {

    @ContributesAndroidInjector
    abstract BaseFragment baseFragment();

    @ContributesAndroidInjector
    abstract SplashFragment splashFragment();

    @ContributesAndroidInjector
    abstract MapFragment mapFragment();

    @ContributesAndroidInjector
    abstract FlightsFragment flightsFragment();

}
