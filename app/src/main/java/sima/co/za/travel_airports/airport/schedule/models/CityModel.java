package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityModel {

    @SerializedName("nameTranslations")
    @Expose
    private String mNameTranslations;

    @SerializedName("nameCity")
    @Expose
    private String mNameCity;

    public String getNameTranslations() {
        return mNameTranslations;
    }

    public void setNameTranslations(String aNameTranslations) {
        mNameTranslations= aNameTranslations;
    }

    public String getNameCity() {
        return mNameCity;
    }

    public void setNameCity(String aNameCity) {
        mNameCity = aNameCity;
    }


}
