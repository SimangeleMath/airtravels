package sima.co.za.travel_airports.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.ProgressBar;

import sima.co.za.travel_airports.R;

public class DialogHelper {
    private Dialog mDialog;
    private Activity mActivity;

    public DialogHelper(Context context) {
        mDialog = new Dialog(context);
        Window window = mDialog.getWindow();
        mActivity = (Activity) context;

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.dialog_progress_bar);
        mDialog.setCancelable(false);
        ProgressBar progressBar = mDialog.findViewById(R.id.progressDialog);

        if (window != null) {
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }

        if (progressBar != null && progressBar.getIndeterminateDrawable() != null) {
            progressBar.getIndeterminateDrawable().setColorFilter(Color.YELLOW, android.graphics.PorterDuff.Mode.SRC_IN);
        }
    }

    /**
     * Shows the progress dialog if the dialog is not currently displayed
     */
    public void showProgress() {
        if (mDialog != null && !mDialog.isShowing() && !mActivity.isDestroyed() && !mActivity.isFinishing()) {
            mDialog.show();
        }
    }


    /**
     * Check if  progress dialog is visible
     */
    public boolean isProgressDialogVisible() {
        if (mDialog != null && !mActivity.isDestroyed() && !mActivity.isFinishing()) {
            return mDialog.isShowing();
        }
        return false;
    }

    /**
     * Hides the progress dialog if the dialog is currently displayed
     */
    public void dismissProgress() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
