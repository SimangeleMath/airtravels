package sima.co.za.travel_airports.di;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import sima.co.za.travel_airports.BuildConfig;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.services.AppInterceptor;
import sima.co.za.travel_airports.services.TravelAirportApiService;
import sima.co.za.travel_airports.utils.PreferencesHelper;
import sima.co.za.travel_airports.utils.Utils;
import sima.co.za.travel_airports.utils.Utils;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Application providesApplication() {
        return application;
    }

    /**
     * Provides the Application Context
     *
     * @return a Context
     */

    @Provides
    Context providesApplicationContext() {
        return application;
    }

    /**
     * The method returns the TravelAirportApiService object
     * @return TravelAirportApiService
     */

    @Provides
    @Singleton
    TravelAirportApiService provideApiService(Retrofit aRetrofit){
        return aRetrofit.create(TravelAirportApiService.class);
    }

    @Provides
    @Singleton
    Interceptor provideInterceptor() {
        return new AppInterceptor();
    }

    @Singleton
    @Provides
    Retrofit provideNetwork(OkHttpClient aOkHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .callFactory(aOkHttpClient)
                .build();
    }

    @Singleton
    @Provides
    OkHttpClient providesOkHttpClient(AppInterceptor aAppInterceptor) {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpClient.addInterceptor(interceptor);
            httpClient.addNetworkInterceptor(new StethoInterceptor());
        }
        httpClient.addInterceptor(aAppInterceptor);
        httpClient.connectTimeout(30, TimeUnit.SECONDS);
        return httpClient.build();
    }

    /*
     * The method returns the Gson object
     * */
    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    /*
     * The method returns the Cache object
     * */
    @Provides
    @Singleton
    Cache provideCache(Application application) {
        long cacheSize = 10 * 1024 * 1024; // 10 MB
        File httpCacheDirectory = new File(application.getCacheDir(), "http-cache");
        return new Cache(httpCacheDirectory, cacheSize);
    }

    @Provides
    @Singleton
    AirportModel provideAirportData() {
        return new AirportModel();
    }

    @Provides
    @Singleton
    Utils provideFileUtils() {
        return new Utils();
    }

    @Provides
    @Singleton
    PreferencesHelper provideSharedPreferences() {
        return new PreferencesHelper(application);
    }

}
