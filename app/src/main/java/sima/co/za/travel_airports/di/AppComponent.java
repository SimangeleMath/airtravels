package sima.co.za.travel_airports.di;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;
import sima.co.za.travel_airports.TravelAirportsApplication;

@Singleton
@Component(modules = {
        AndroidSupportInjectionModule.class,
        AppModule.class,
        ActivityModule.class,
        RepositoryModule.class,
        FragmentBuilder.class,
        ViewModelModule.class,
        ServiceBuilderModule.class,
})
public interface AppComponent {

    void inject(TravelAirportsApplication application);

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(TravelAirportsApplication application);
        Builder repoModule(RepositoryModule aRepositoryModule);
        Builder appModule(AppModule aAppModule);
        Builder serviceModule(ServiceBuilderModule aServiceBuilderModule);
        AppComponent build();

    }
}
