package sima.co.za.travel_airports.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.navigation.NavDirections;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import pub.devrel.easypermissions.EasyPermissions;
import sima.co.za.travel_airports.R;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.airport.AirportViewModel;
import sima.co.za.travel_airports.base.BaseFragment;
import sima.co.za.travel_airports.databinding.FragmentMapBinding;
import sima.co.za.travel_airports.di.ViewModelFactory;
import sima.co.za.travel_airports.services.LocationService;
import sima.co.za.travel_airports.utils.Constants;
import sima.co.za.travel_airports.utils.DialogHelper;
import sima.co.za.travel_airports.utils.PreferencesHelper;
import sima.co.za.travel_airports.utils.Utils;

public class MapFragment extends BaseFragment implements OnMapReadyCallback {

    private static String TAG = MapFragment.class.getSimpleName();
    private FragmentMapBinding mBinding;
    private GoogleMap mGoogleMap;
    public static final int REQUEST_CODE_LOCATION_PERMISSION = 2;
    private LatLng mCurrentLocation;
    private PreferencesHelper mSharedPreferences;

    @Inject
    ViewModelFactory mFactory;
    @Inject
    Utils mUtils;
    private AirportModel mAirportModel;
    private AirportViewModel mAirportViewModel;
    private DialogHelper mCustomProgressDialog;
    private final BroadcastReceiver locationsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            double mLatitude = intent.getDoubleExtra(LocationService.EXTRA_LATITUDE, 0);
            double mLongitude = intent.getDoubleExtra(LocationService.EXTRA_LONGITUDE, 0);
            if (mLatitude != 0 && mLongitude != 0) {
                mCurrentLocation = new LatLng(mLatitude, mLongitude);
                if (mCurrentLocation.longitude != 0 & mCurrentLocation.latitude != 0) {
                    mAirportViewModel.getAirports(mCurrentLocation);
                }

            }
        }
    };

    public MapFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle aSavedInstanceState) {
        super.onCreate(aSavedInstanceState);
        mAirportViewModel = ViewModelProviders.of(this, mFactory).get(AirportViewModel.class);
        mAirportModel = new AirportModel();
        mCustomProgressDialog = new DialogHelper(requireContext());
        mSharedPreferences = new PreferencesHelper(requireContext());

        mAirportViewModel.getMutableLiveData().observe(this, new Observer<List<AirportModel>>() {
            @Override
            public void onChanged(@Nullable List<AirportModel> airportModels) {
                if (mCustomProgressDialog.isProgressDialogVisible())
                    mCustomProgressDialog.dismissProgress();
                plotPoint(airportModels, true);
            }
        });

        receiveBroadcast();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentMapBinding.inflate(inflater, container, false);
        mBinding.mapView.onCreate(savedInstanceState);
        mBinding.mapView.onResume();
        mBinding.mapView.getMapAsync(this);
        initializeMap();
        return mBinding.getRoot();
    }

    private void initializeMap() {
        mBinding.fabCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                focusOnCurrentLocation();
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(requireActivity()).registerReceiver(locationsReceiver, new IntentFilter(LocationService.ACTION_LOCATION_BROADCAST));
        if (mSharedPreferences.getCoordinate("LAT") != 0 && mSharedPreferences.getCoordinate("LNG") != 0) {
            mCurrentLocation = new LatLng(mSharedPreferences.getCoordinate("LAT"), mSharedPreferences.getCoordinate("LNG"));
            mCustomProgressDialog.showProgress();
            mAirportViewModel.getAirports(mCurrentLocation);
        }
    }

    private void receiveBroadcast() {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap aGoogleMap) {
        mGoogleMap = aGoogleMap;
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);
        mGoogleMap.setMinZoomPreference(5.0f);
        mGoogleMap.setMaxZoomPreference(30.0f);
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    /**
     * Plot multiple point in Google Map
     */
    public void plotPoint(final List<AirportModel> airportPlotPoint, boolean isAnimateCamera) {
        if (mGoogleMap != null) {
            try {
                mGoogleMap.clear();
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                ArrayList<Marker> markers = new ArrayList<>();
                LatLngBounds.Builder builder = new LatLngBounds.Builder();

                MarkerOptions mMarkerOptions;
                for (int i = 0; i < airportPlotPoint.size(); i++) {
                    mMarkerOptions = new MarkerOptions().position(new LatLng(airportPlotPoint.get(i).getLatitudeAirport(), airportPlotPoint.get(i).getLongitudeAirport()));
                    mMarkerOptions.title(airportPlotPoint.get(i).getNameAirport());
                    mMarkerOptions.flat(true);
                    builder.include(new LatLng(airportPlotPoint.get(i).getLatitudeAirport(), airportPlotPoint.get(i).getLongitudeAirport()));
                    markers.add(mGoogleMap.addMarker(mMarkerOptions));
                    markers.get(i).setTag(airportPlotPoint.get(i));
                }

                if (isAnimateCamera) {
                    //Building our boundaries
                    LatLngBounds mBounds = builder.build();
                    int padding = 10;
                    if (getActivity() != null && isAdded()) {
                        int width = getResources().getDisplayMetrics().widthPixels;
                        padding = (int) (width * 0.40);
                    }
                    Drawable circleDrawable = getResources().getDrawable(R.drawable.circle_shape);
                    BitmapDescriptor markerIcon = getMarkerIconFromDrawable(circleDrawable);

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(mBounds, padding);
                    mGoogleMap.moveCamera(cu);
                    mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
                    //Set bounds radius
//                    mGoogleMap.addGroundOverlay(new GroundOverlayOptions()
//                            .positionFromBounds(mBounds)
//                            .image(markerIcon));

                }

                mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        //setting clicked marker object
                        marker.getTag();
                        mAirportModel = (AirportModel) marker.getTag();

                        NavDirections navDirections = null;
                        if (mAirportModel != null) {
                            navDirections = MapFragmentDirections.actionMapFragmentToFlightsFragment(mAirportModel.getCodeIataAirport(), mAirportModel.getCodeIataCity());
                        }
                        navigateToNextScreen(navDirections);
                        return false;
                    }
                });

            } catch (Exception ex) {
                Log.e(TAG, ex.getMessage());
            }

        }
    }

    private BitmapDescriptor getMarkerIconFromDrawable(Drawable drawable) {
        Canvas canvas = new Canvas();
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        canvas.setBitmap(bitmap);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        drawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    private void focusOnCurrentLocation() {
        if (mCurrentLocation != null) {
            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mCurrentLocation.latitude, mCurrentLocation.longitude));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(11);
            mGoogleMap.moveCamera(center);
            mGoogleMap.animateCamera(zoom);
        }
    }

    @Override
    public void onDestroy() {
        requireActivity().stopService(new Intent(requireContext(), LocationService.class));
        LocalBroadcastManager.getInstance(requireContext()).unregisterReceiver(locationsReceiver);
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        if (mGoogleMap != null) {
            mGoogleMap.clear();
        }
        super.onDestroyView();
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(requireContext());
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(requireActivity(), status, 2404).show();
            }
            return false;
        }
        return true;
    }

}
