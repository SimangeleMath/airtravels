package sima.co.za.travel_airports.utils;

import sima.co.za.travel_airports.services.LocationService;

public class Constants {


    // DEBUGGING
    public static boolean debugging = true;
    public static boolean enableStrictMode = false;
    public static boolean showDebugMessages = debugging;
    public static boolean showInfoMessages = debugging;
    public static boolean showWarningMessages = debugging;
    public static boolean showErrorMessages = debugging;
    public static boolean showStackTrace = debugging;

    // TIMESTAMP FORMAT
    public static String TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() + "LocationBroadcast";
}
