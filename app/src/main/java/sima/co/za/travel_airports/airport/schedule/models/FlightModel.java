package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FlightModel {

    @SerializedName("number")
    @Expose
    private String mNumber;
    @SerializedName("iataNumber")
    @Expose
    private String mIataNumber;
    @SerializedName("icaoNumber")
    @Expose
    private String mIcaoNumber;

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String aNumber) {
        mNumber = aNumber;
    }

    public String getIataNumber() {
        return mIataNumber;
    }

    public void setIataNumber(String aIataNumber) {
        mIataNumber = aIataNumber;
    }

    public String getIcaoNumber() {
        return mIcaoNumber;
    }

    public void setIcaoNumber(String aIcaoNumber) {
        this.mIcaoNumber = aIcaoNumber;
    }

}