package sima.co.za.travel_airports.airport;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AirportModel {

    @SerializedName("timezone")
    @Expose
    private String mTimezone;
    @SerializedName("distance")
    @Expose
    private float mDistance;
    @SerializedName("nameAirport")
    @Expose
    private String mNameAirport;
    @SerializedName("codeIataAirport")
    @Expose
    private String mCodeIataAirport;
    @SerializedName("codeIcaoAirport")
    @Expose
    private String mCodeIcaoAirport;
    @SerializedName("nameTranslations")
    @Expose
    private String mNameTranslations;
    @SerializedName("latitudeAirport")
    @Expose
    private double mLatitudeAirport;
    @SerializedName("longitudeAirport")
    @Expose
    private double mLongitudeAirport;
    @SerializedName("GMT")
    @Expose
    private int mGMT;
    @SerializedName("phone")
    @Expose
    private String mPhone;
    @SerializedName("nameCountry")
    @Expose
    private String mNameCountry;
    @SerializedName("codeIso2Country")
    @Expose
    private String mCodeIso2Country;
    @SerializedName("codeIataCity")
    @Expose
    private String mCodeIataCity;


    public String getTimezone() {
        return mTimezone;
    }

    public void setTimezone(String aTimezone) {
        mTimezone = aTimezone;
    }

    public float getDistance() {
        return mDistance;
    }

    public void setDistance(float aDistance) {
        mDistance = aDistance;
    }

    public String getNameAirport() {
        return mNameAirport;
    }

    public void setNameAirport(String aNameAirport) {
        mNameAirport = aNameAirport;
    }

    public String getCodeIataAirport() {
        return mCodeIataAirport;
    }

    public void setCodeIataAirport(String aCodeIataAirport) {
        mCodeIataAirport = aCodeIataAirport;
    }

    public String getCodeIcaoAirport() {
        return mCodeIcaoAirport;
    }

    public void setCodeIcaoAirport(String aCodeIcaoAirport) {
        mCodeIcaoAirport = aCodeIcaoAirport;
    }

    public String getNameTranslations() {
        return mNameTranslations;
    }

    public void setNameTranslations(String aNameTranslations) {
        mNameTranslations = aNameTranslations;
    }

    public double getLatitudeAirport() {
        return mLatitudeAirport;
    }

    public void setLatitudeAirport(double aLatitudeAirport) {
        mLatitudeAirport = aLatitudeAirport;
    }

    public double getLongitudeAirport() {
        return mLongitudeAirport;
    }

    public void setLongitudeAirport(double aLongitudeAirport) {
        mLongitudeAirport = aLongitudeAirport;
    }

    public int getGMT() {
        return mGMT;
    }

    public void setGMT(int aGMT) {
        mGMT = aGMT;
    }

    public String getPhone() {
        return mPhone;
    }

    public void setPhone(String aPhone) {
        mPhone = aPhone;
    }

    public String getNameCountry() {
        return mNameCountry;
    }

    public void setNameCountry(String aNameCountry) {
        mNameCountry = aNameCountry;
    }

    public String getCodeIso2Country() {
        return mCodeIso2Country;
    }

    public void setCodeIso2Country(String aCodeIso2Country) {
        mCodeIso2Country = aCodeIso2Country;
    }

    public String getCodeIataCity() {
        return mCodeIataCity;
    }

    public void setCodeIataCity(String aCodeIataCity) {
        mCodeIataCity = aCodeIataCity;
    }

}