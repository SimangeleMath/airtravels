package sima.co.za.travel_airports.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import pub.devrel.easypermissions.EasyPermissions;
import sima.co.za.travel_airports.utils.Constants;
import sima.co.za.travel_airports.utils.PreferencesHelper;

public class LocationService extends Service implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = LocationService.class.getSimpleName();

    private static final long UPDATE_INTERVAL = 10 * 1000;  /* 10 secs */
    private static final long FASTEST_INTERVAL = 2000; /* 2 sec */

    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();
    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() + "LocationBroadcast";

    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";

    PreferencesHelper preferencesHelper;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (EasyPermissions.hasPermissions(this, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // GPS location can be null if GPS is switched off
                            if (location != null) {
                                onLocationChanged(location);
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (Constants.showDebugMessages)Log.d(TAG, "Error trying to get last GPS location");
                            e.printStackTrace();
                        }
                    });

            Log.d(TAG, "Connected to Google API");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (Constants.showDebugMessages) Log.d(TAG, "Connection suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (Constants.showDebugMessages) Log.d(TAG, "Failed to connect to Google API");
    }

    @Override
    public void onLocationChanged(Location aLocation) {
        if (Constants.showDebugMessages) Log.d(TAG, "onLocationChanged: " + aLocation);
        if (aLocation != null) {
            //Send result to activities
            sendMessageToUI(aLocation.getLatitude(), aLocation.getLongitude());
        }
    }

    @Override
    public void onStatusChanged(String provider, int i, Bundle bundle) {
        if (Constants.showDebugMessages) Log.d(TAG, "onStatusChanged: " + provider);
    }

    @Override
    public void onProviderEnabled(String provider) {
        if (Constants.showDebugMessages) Log.d(TAG, "onProviderEnabled: " + provider);
    }

    @Override
    public void onProviderDisabled(String provider) {
        if (Constants.showDebugMessages) Log.d(TAG, "onProviderDisabled: " + provider);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY; //by default
        //PRIORITY_BALANCED_POWER_ACCURACY, PRIORITY_LOW_POWER, PRIORITY_NO_POWER are the other priority modes

        mLocationRequest.setPriority(priority);
        mLocationClient.connect();

        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    private void sendMessageToUI(double aLat, double aLng) {
        if (Constants.showDebugMessages) Log.d(TAG, "Sending Location");
        preferencesHelper = new PreferencesHelper(getApplicationContext());
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, aLat);
        intent.putExtra(EXTRA_LONGITUDE, aLng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        preferencesHelper.putCo("LAT", String.valueOf(aLat));
        preferencesHelper.putCo("LNG", String.valueOf(aLng));
    }

}
