package sima.co.za.travel_airports.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import androidx.navigation.NavController;
import androidx.navigation.NavDirections;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import dagger.android.support.AndroidSupportInjection;
import sima.co.za.travel_airports.R;


public class BaseFragment extends Fragment {

    protected NavController mNavController;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        try {
            AndroidSupportInjection.inject(this);

        } catch (IllegalArgumentException aE) {
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mNavController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
    }

    public void navigateToNextScreen(int aResId, Bundle aArguments, NavOptions aNavOptions) {
        mNavController.navigate(aResId, aArguments, aNavOptions);
    }

    public void navigateToNextScreen(int aResId, NavOptions aNavOptions ) {
        navigateToNextScreen(aResId, null, aNavOptions);
    }

    public void navigateToNextScreen(NavDirections navDirections) {
        mNavController.navigate(navDirections);
    }

}
