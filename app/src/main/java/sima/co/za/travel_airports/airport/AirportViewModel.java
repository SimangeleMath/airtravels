package sima.co.za.travel_airports.airport;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import javax.inject.Inject;

import sima.co.za.travel_airports.BuildConfig;
import sima.co.za.travel_airports.models.AirportRequestModel;

public class AirportViewModel extends ViewModel{

    private final Application mApplication;
    private final AirportRepository mAirportRepository;
    private final MutableLiveData<List<AirportModel>> mMutableLiveData = new MutableLiveData<>();

    private final Observer<List<AirportModel>> response = new Observer<List<AirportModel>>() {
        @Override
        public void onChanged(@Nullable List<AirportModel> aResponse) {
            if (aResponse != null) {
                mMutableLiveData.setValue(aResponse);
            }
        }
    };

    @Inject
    public AirportViewModel(Application application, AirportRepository aAirportRepository) {
        mApplication = application;
        mAirportRepository = aAirportRepository;
        mAirportRepository.getMutableLiveData().observeForever(response);
    }

    public void getAirports(LatLng mCurrentLocation){
        mAirportRepository.getAirports(new AirportRequestModel(BuildConfig.API_KEY, mCurrentLocation.latitude, mCurrentLocation.longitude,100));
    }

    public MutableLiveData<List<AirportModel>> getMutableLiveData() {
        return mMutableLiveData;
    }
}
