package sima.co.za.travel_airports.services;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;
import sima.co.za.travel_airports.airport.schedule.models.CityModel;

public interface TravelAirportApiService {

    @Headers("Content-Type: application/json")
    @GET("nearby")
    Call<List<AirportModel>> executeGetNearByAirports(
            @Query("key") String key,
            @Query("lat") double lat,
            @Query("lng") double lng,
            @Query("distance") int distance);

    @Headers("Content-Type: application/json")
    @GET("timetable")
    Call<List<AirportScheduleModel> > executeGetAirportSchedule(
            @Query("key") String key,
            @Query("iataCode") String iataCode,
            @Query("type") String type);

    @Headers("Content-Type: application/json")
    @GET("cityDatabase")
    Call<List<CityModel>> executeGetCity(
            @Query("key") String key,
            @Query("codeIataCity") String codeIataCity);
}
