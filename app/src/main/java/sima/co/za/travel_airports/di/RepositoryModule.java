package sima.co.za.travel_airports.di;

import android.app.Application;

import dagger.Module;
import dagger.Provides;
import sima.co.za.travel_airports.airport.AirportModel;
import sima.co.za.travel_airports.airport.AirportRepository;
import sima.co.za.travel_airports.airport.schedule.DepartureRepository;
import sima.co.za.travel_airports.services.TravelAirportApiService;

@Module
public class RepositoryModule {

    @Provides
    AirportRepository provideAirportRepository(Application aApplication, TravelAirportApiService aApiService) {
        return new AirportRepository(aApplication, aApiService);
    }
    @Provides
    DepartureRepository provideAirportScheduleRepository(Application aApplication, TravelAirportApiService aApiService, AirportModel aAirportModel) {
        return new DepartureRepository(aApplication, aApiService, aAirportModel);
    }
}
