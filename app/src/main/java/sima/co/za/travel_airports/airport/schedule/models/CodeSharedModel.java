package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CodeSharedModel {

    @SerializedName("airline")
    @Expose
    private AirLineModel mAirline;
    @SerializedName("flight")
    @Expose
    private FlightModel mFlight;

    public AirLineModel getAirline() {
        return mAirline;
    }

    public void setAirline(AirLineModel aAirline) {
        mAirline = aAirline;
    }

    public FlightModel getFlight() {
        return mFlight;
    }

    public void setFlight(FlightModel aFlight) {
        mFlight = aFlight;
    }

}