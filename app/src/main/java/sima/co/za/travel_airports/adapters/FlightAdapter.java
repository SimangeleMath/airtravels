package sima.co.za.travel_airports.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import sima.co.za.travel_airports.R;
import sima.co.za.travel_airports.airport.schedule.models.AirportScheduleModel;
import sima.co.za.travel_airports.airport.schedule.models.CityModel;
import sima.co.za.travel_airports.databinding.ItemFlightBinding;
import sima.co.za.travel_airports.utils.Utils;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

    private List<AirportScheduleModel> mObjectList = new ArrayList<>();
    private CityModel mCityModel = new CityModel();
    private ItemFlightBinding mBinding;
    private Utils mUtils;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        mBinding = ItemFlightBinding.inflate(layoutInflater, parent, false);

        return new ViewHolder(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mObjectList.get(position));
        holder.bindCity(mCityModel);
        AirportScheduleModel scheduleModel = mObjectList.get(position);
        mUtils = new Utils();
        if (scheduleModel.getStatus().equals("departure")) {
            holder.mBinding.imgStatus.setImageResource(R.drawable.red_dot_x1);
        } else {
            holder.mBinding.imgStatus.setImageResource(R.drawable.green_dot_x1);
        }

        try {
            holder.mBinding.txtDepartureTime.setText(mUtils.formatTime(scheduleModel.getDeparture().getActualTime()));
        }catch (Exception ex){
            Log.e("FlightAdapter",ex.getMessage());
        }

    }

    @Override
    public int getItemCount() {
        return mObjectList.size();
    }

    public void setScheduleList(List<AirportScheduleModel> aList) {
        mObjectList.clear();
        mObjectList.addAll(aList);
        notifyItemRangeChanged(0, aList.size());
    }

    public void setCity(CityModel aCty) {
        mCityModel = aCty;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private ItemFlightBinding mBinding;

        ViewHolder(ItemFlightBinding aBinding) {
            super(aBinding.getRoot());
            mBinding = aBinding;
            mBinding.executePendingBindings();
        }

        void bind(final AirportScheduleModel row) {
            mBinding.setAirportScheduleModel(row);
        }

        void bindCity(final CityModel row) {
            mBinding.setCityModel(row);
        }
    }
}

