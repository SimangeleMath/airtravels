package sima.co.za.travel_airports.activities;

import android.content.Intent;
import android.os.Bundle;

import sima.co.za.travel_airports.R;
import sima.co.za.travel_airports.base.BaseActivity;
import sima.co.za.travel_airports.services.LocationService;


public class MainActivity extends BaseActivity {

    private boolean mAlreadyStartedService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!mAlreadyStartedService) {
            Intent intent = new Intent(this, LocationService.class);
            startService(intent);
            mAlreadyStartedService = true;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mAlreadyStartedService = false;
    }
}