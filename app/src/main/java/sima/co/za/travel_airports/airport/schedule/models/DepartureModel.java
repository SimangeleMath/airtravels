package sima.co.za.travel_airports.airport.schedule.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepartureModel{
    @SerializedName("iataCode")
    @Expose
    private String mIataCode;
    @SerializedName("icaoCode")
    @Expose
    private String mIcaoCode;
    @SerializedName("terminal")
    @Expose
    private String mTerminal;
    @SerializedName("scheduledTime")
    @Expose
    private String mScheduledTime;
    @SerializedName("estimatedTime")
    @Expose
    private String mEstimatedTime;
    @SerializedName("actualTime")
    @Expose
    private String mActualTime;
    @SerializedName("gate")
    @Expose
    private String mGate;

    public String getIataCode() {
        return mIataCode;
    }

    public void setIataCode(String aIataCode) {
        mIataCode = aIataCode;
    }

    public String getIcaoCode() {
        return mIcaoCode;
    }

    public void setIcaoCode(String aIcaoCode) {
        mIcaoCode = aIcaoCode;
    }

    public String getTerminal() {
        return mTerminal;
    }

    public void setTerminal(String aTerminal) {
        mTerminal = aTerminal;
    }

    public String getScheduledTime() {
        return mScheduledTime;
    }

    public void setScheduledTime(String aScheduledTime) {
        mScheduledTime = aScheduledTime;
    }

    public String getEstimatedTime() {
        return mEstimatedTime;
    }

    public void setEstimatedTime(String aEstimatedTime) {
        mEstimatedTime = aEstimatedTime;
    }

    public String getActualTime() {
        return mActualTime;
    }

    public void setActualTime(String aActualTime) {
        mActualTime = aActualTime;
    }

    public String getGate() {
        return mGate;
    }

    public void setGate(String aGate) {
        mGate = aGate;
    }

}