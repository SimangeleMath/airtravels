# Airtravels
Airtravel is an airport application displaying nearby airports based on user current location and flights details.

## Getting Started
Created 2 product flavors

demoDebug points to url:
* http://5ca362aa8bae720014a9620e.mockapi.io/
* mocks the api response (unlimited) 
fullDebug point to url:
* https://aviation-edge.com/v2/public/

## Authors
* **Simangele Mathenjwa**
